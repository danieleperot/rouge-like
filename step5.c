#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define PLAYER_HEALTH 10
#define PLAYER_STRENGTH 6
#define MONSTER_MAX_HEALTH 15
#define MONSTER_MAX_STRENGTH 8

typedef struct{
    int x;
    int y;
    int health;
    int strength;
} player;

typedef struct {
    int x;
    int y;
	int health;
    int strength;
} monster;


monster create_monster(int max_health, int max_strength);
void combat(player *p, monster *m);

int main()
{
	player p;
	monster m; 

	srand(time(NULL));
	p.health = PLAYER_HEALTH;
	p.strength = PLAYER_STRENGTH;
	m = create_monster(MONSTER_MAX_HEALTH,MONSTER_MAX_STRENGTH);

	printf("You are fighting a monster with strength %d and health %d\n",m.health,m.strength);
	do 
	{
		combat(&p,&m);

	} while (p.health > 0 && m.health > 0);

	if (p.health > 0)
		printf("You survived!\n");
	else
		printf("You died to the monster of the dungeon, your spoils will never be found!\n");

}

monster create_monster(int max_health, int max_strength)
{
	monster m;
	m.health = rand()%max_health+1;
	m.strength = rand()%max_strength+1;

	return m;
}

void combat(player *p, monster *m)
{
	// se first = 0, il player attacca per primo, altrimenti inizia il mostro
	int first = rand()%2;
	if (first==0) //inizia il player
	{
		m->health = m->health - rand()%(p->strength+1);
		if (m->health >= 0) //se il mostro è sopravvissuto
			p->health = p->health - rand()%(m->strength+1);
	}
	else 
	{
		p->health = p->health - rand()%(m->strength+1);
		if (p->health >= 0) //se il player è sopravvissuto
			m->health = m->health - rand()%(p->strength+1);		
	}
}