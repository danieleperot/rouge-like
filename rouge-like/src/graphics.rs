use crate::game::Game;
use crate::map::Tile;

pub trait Graphics {
    fn draw(self: &Self, game: Game) -> ();
}

pub struct Console {}

impl Console {
    pub fn new() -> Self { Console {} }

    fn print_wall(index: usize, element: &Tile, game: &Game) {
        let _r = game.map().rows();
        let _c = game.map().columns();

        let position = game.map().index_to_coordinate(index);
        if position.y == 0 { println!(); };

        print!("{}", match element {
            _ if game.is_player(position.x, position.y) => "@",
            Tile::Wall => "░",
            Tile::Hall => " ",
        });
    }
}

impl Graphics for Console {
    fn draw(self: &Self, game: Game) -> () {
        let map = game.map();

        for (index, element) in map.tiles().iter().enumerate() {
            Console::print_wall(index, element, &game)
        }
    }
}
