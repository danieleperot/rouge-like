use game::Game;
use crate::graphics::Console;
use crate::graphics::Graphics;

mod map;
mod game;
mod graphics;


fn main() {
    const COLUMNS: usize = 15;
    const ROWS: usize = 15;

    let game = Game::new(COLUMNS, ROWS);
    let console = Console::new();
    console.draw(game)
}
