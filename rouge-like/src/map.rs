use rand::Rng;

#[derive(PartialEq)]
pub enum Tile {
    Wall,
    Hall
}

pub struct Map {
    tiles: Vec<Tile>,
    rows: usize,
    columns: usize
}

impl Map {
    pub fn new(columns: usize, rows: usize) -> Self {
        Map {
            tiles: generate_tiles(columns, rows),
            rows,
            columns
        }
    }

    pub fn tiles(self: &Self) -> &Vec<Tile> {
        &self.tiles
    }

    pub fn rows(self: &Self) -> usize {
        self.rows.clone()
    }

    pub fn columns(self: &Self) -> usize {
        self.columns.clone()
    }

    pub fn find_free_tile(self: &Self) -> Option<Coordinate> {
        for _ in 0..self.tiles.len() {
            let random_index = rand::thread_rng().gen_range(0..self.tiles.len());
            let value = self.tiles.get(random_index).unwrap();

            if *value == Tile::Hall {
                return Some(self.index_to_coordinate(random_index));
            }
        }

        return None
    }

    pub fn index_to_coordinate(self: &Self, index: usize) -> Coordinate {
        Coordinate::new(index / self.rows, index % self.rows)
    }
}

fn in_range(value: &usize, min: usize, max: usize) -> bool {
    *value > min && *value < (max - 1)
}

fn generate_tiles(columns: usize, rows: usize) -> Vec<Tile> {
    let mut map = vec![];

    for y in 0..columns {
        for x in 0..rows {
            map.push(add_wall(x, y, rows, columns));
        }
    }

    map
}

fn add_wall(x: usize, y: usize, max_x: usize, max_y: usize) -> Tile {
    let gen_wall = rand::thread_rng().gen_range(0..100) > 25;

    if in_range(&x, 0, max_x) && in_range(&y, 0, max_y) && gen_wall {
        return Tile::Hall;
    }

    Tile::Wall
}

#[derive(Eq, PartialEq, Debug)]
pub struct Coordinate {
    pub x: usize,
    pub y: usize
}

impl Coordinate {
    pub fn new(x: usize, y: usize) -> Self {
        Coordinate { x, y }
    }
}
