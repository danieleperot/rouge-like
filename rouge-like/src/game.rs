use crate::map::{Map, Coordinate};

pub struct Game {
    map: Map,
    player: Player
}

impl Game {
    pub fn new(columns: usize, rows: usize) -> Self {
        let map = Map::new(columns, rows);
        let player_pos = match map.find_free_tile() {
            Some(position) => position,
            None => Coordinate::new(0, 0)
        };

        Game { map, player: Player { position: player_pos } }
    }

    pub fn is_player(self: &Self, x: usize, y: usize) -> bool {
        self.player.position == Coordinate { x, y }
    }

    pub fn map(self: &Self) -> &Map {
        &self.map
    }
}

struct Player {
    position: Coordinate
}
