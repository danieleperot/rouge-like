#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ID_FLOOR 0
#define ID_WALL 1
#define ID_PLAYER 2
#define ID_EXIT 3
#define ID_MONSTER 4

#define C_FLOOR  ' '
#define C_WALL   '#'
#define C_PLAYER 'K'
#define C_EXIT   'O'
#define C_MONSTER  '@'
#define C_UNDEF  '?'

#define WIN_SIZE 20

#define PLAYER_HEALTH 10
#define PLAYER_STRENGTH 6
#define MONSTER_MAX_HEALTH 15
#define MONSTER_MAX_STRENGTH 8


typedef struct{
    int x;
    int y;
    int health;
    int strength;
} player;

typedef struct {
    int x;
    int y;
    int health;
    int strength;
} monster;

typedef monster data;

struct nodo
{
  data el;
  struct nodo *next;
};

typedef struct nodo *lista;

typedef enum {down,right,up,left} direzione;

/*funzioni sulle liste*/
int length(lista l);
lista get (lista l, int x, int y);
void add(lista *l, data el);
void delete(lista *l, int x, int y);

int is_in(monster m, int x, int y);
monster create_monster(int max_health, int max_strength);
void spawn(int **map, int size, lista *monsters);

char map2char(int **map, int size, int x, int y);
void visualize_map(int** map, int size, int px, int py, int win_size);
void get_next_location(direzione d, int step, int x, int y, int* xt, int *yt);
int **load_map(const char *filename, int *size);
void find_player(int **map, int size, int *px, int *py);
direzione char2dir(char a);

int main()
{
	int **map, size, nx, ny, game_time=0, exit_found=0;
    direzione d;
    char nome[50],action;
    player p;
    lista monsters=NULL;

    srand(time(NULL));

    printf("Map name: ");
	scanf("%s%*c", nome);

    map = load_map(nome,&size);	

    find_player(map,size,&p.x,&p.y);
    p.health = PLAYER_HEALTH;
    p.strength = PLAYER_HEALTH;

    do
    {

        //prova a fare lo spawn di un mostro
        spawn(map, size, &monsters);
        
        printf("\n\n");
        visualize_map(map,size,p.x,p.y,WIN_SIZE);
        printf("\n\n");
        printf("Time: %d\n", game_time);
        printf("Choose your next action (wasd - q to exit): ");
        scanf("%c%*c", &action);        
        d = char2dir(action);
        //se l'azione è valida
        if (d>=down && d<=left)
        {
            get_next_location(d, 1, p.x, p.y, &nx, &ny);            
            // controllo se ho trovato l'uscita
            if (map[ny][nx] == ID_EXIT)
                exit_found = 1;
            // altrimenti mi sposto solo se la nuova destinazione è vuota
            else if (map[ny][nx]==ID_FLOOR) 
            {
                map[p.y][p.x] = ID_FLOOR;  //"togliamo" il player dalla mappa
                p.x = nx;
                p.y = ny;
                map[p.y][p.x] = ID_PLAYER;  //"rimettiamo" il player sulla mappa
            }

            // aggiorno il tempo di gioco
            game_time++;            

        }

    } while (action!='q' && exit_found == 0);

    printf("\n\nGAME OVER!\n");
    if (exit_found)
        printf("You found the exit of the dungeon in %d turns!\n", game_time);
    else
        printf("You gave up!\n");
    
	return 0;
}

int **load_map(const char *filename, int *size)
{
    int x, y, **map;
    FILE *f = NULL;

    // Open the file and populate the map
    printf("Apro il file %s per caricare la mappa.\n", filename);
    f = fopen(filename, "r");

    if (f == NULL)
    {
    	printf("Errore: non riesco ad aprire il file %s\n",filename);
    	return NULL;
    }

    fscanf(f,"%d",size);
    
    map = calloc(*size,sizeof(int*));
    for (y=0; y<*size; y++)
    	map[y] = calloc(*size,sizeof(int));

    for (y=0; y<*size; y++)
    	for (x=0; x<*size; x++)
    		fscanf(f,"%d",&map[y][x]);

    fclose(f);

    return map;
}

char map2char(int **map, int size, int x, int y)
{
	if (map[y][x] == ID_FLOOR)
		return C_FLOOR;
	else if (map[y][x] == ID_WALL)
		return C_WALL;
	else if (map[y][x] == ID_PLAYER)
		return C_PLAYER;
	else if (map[y][x] == ID_EXIT)
		return C_EXIT;
    else if (map[y][x] == ID_MONSTER)
        return C_MONSTER;
	else
		return C_UNDEF;
}

void visualize_map(int** map, int size, int px, int py, int win_size)
{
	int x,y,w = win_size/2;
    
    for(y=py-w; y<=py+w; y++)
    {          
        for(x=px-w; x<=px+w; x++)
        {
            if (x<0 || x>=size || y<0 || y>=size)
                printf("%c",C_FLOOR);
            else
                printf("%c",map2char(map,size,x,y));
        }
        printf("\n");
    }    
}



void get_next_location(direzione d, int step, int x, int y, int* xt, int *yt)
{
    switch(d)
    {
        case down:
            *xt = x; *yt = y+step;
            break;
        case right:
            *xt = x+step; *yt = y;
            break;
        case up:
            *xt = x; *yt = y-step;
            break;
        case left:
            *xt = x-step; *yt = y;
            break;
    }
}


void find_player(int **map, int size, int *px, int *py)
{
    int x,y;
    for (y=0; y<size; y++)
        for (x=0; x<size; x++)
            if (map[x][y]==ID_PLAYER)
            {
                *px = x;
                *py = y;
                break;
            }
}

direzione char2dir(char a)
{
    switch(a)
    {
        case 'w':
            return up;
        case 'a':
            return left;
        case 's':
            return down;            
        case 'd':
            return right;
        default:
            return -1;
    }   
}

int length(lista l)
{
    if (l==NULL)
        return 0;
    else
        return 1 + length (l->next);
}

lista get(lista l, int x, int y)
{
    if (l==NULL)
        return NULL;
    else if(is_in(l->el,x,y))
        return l;
    else
        return get(l->next,x,y);   
}

void add(lista *l, data el)
{
    if (*l == NULL)
    {
        lista p = malloc(sizeof(struct nodo));
        p->el = el;
         p->next = *l;
        *l = p;
    }
    else
        add(&((*l)->next),el);
}

void delete(lista *l, int x, int y)
{
    if (*l != NULL)
    {
        if (is_in((*l)->el,x,y))
        {
            lista old = *l;
            *l = old->next;
            free(old);
        }
        else
            return delete(&((*l)->next),x,y);
    }    
}

int is_in(monster m, int x, int y)
{
    return (m.x==x && m.y==y);
}

monster create_monster(int max_health, int max_strength)
{
    monster m;
    m.health = rand()%max_health+1;
    m.strength = rand()%max_strength+1;

    return m;
}

void spawn(int **map, int size, lista *monsters)
{
    int x,y;
    
    //genero x e y compresi fra 1 e size-1 (perchè i bordi sono sicuramente occupati da muri)
    x = 1 + rand()%(size-1);
    y = 1 + rand()%(size-1);

    //se la cella scelta casualmente è vuota, genero il mostro
    if (map[y][x] == ID_FLOOR)
    {
    	monster m = create_monster(MONSTER_MAX_HEALTH,MONSTER_MAX_STRENGTH);
        m.x = x;
        m.y = y;
        add(monsters,m);
        map[y][x] = ID_MONSTER;
        //printf("Added a monster with strength %d and health %d\n",m.strength,m.health);
    } 

}